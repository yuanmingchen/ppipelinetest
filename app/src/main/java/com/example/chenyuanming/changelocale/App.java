package com.example.chenyuanming.changelocale;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

/**
 * Created by chenyuanming on 21/03/2018.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        LocaleUtil.initAppLanguage(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleUtil.wrap(base));
    }

    public void onConfigurationChanged(Configuration newConfig) {
        LocaleUtil.onConfigurationChanged(this, newConfig);
        super.onConfigurationChanged(newConfig);
    }
}
