package com.example.chenyuanming.changelocale;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;


import java.util.Locale;

public class LocaleUtil {
    public static final String LANGUAGE_AUTO = "auto";
    public static final String LANGUAGE_CHINESE = "zh";
    public static final String LANGUAGE_ENGLISH = "en";
    private static final String CURRENT_LANGUAGE = "CURRENT_LANGUAGE";


    public static boolean isAuto(Context context) {
        return LANGUAGE_AUTO.equals(SPUtils.getString(context, CURRENT_LANGUAGE, LANGUAGE_AUTO));
    }

    /**
     * 设置语言：如果之前有设置就遵循设置如果没设置过就跟随系统语言
     */
    public static void initAppLanguage(Context context) {
        if (context == null) return;
        //保存自定义配置前，系统默认值
        Locale myLocale = getUserLocale(context);
        // 本地语言设置
        if (needUpdateLocale(context, myLocale)) {
            updateLocale(context, myLocale);
        }
    }

    /**
     * 获取OS的Locale
     *
     * @return Locale
     */
    @NonNull
    public static Locale getSystemLocale() {
        return Resources.getSystem().getConfiguration().locale;
    }


    /**
     * 获取当前的Locale
     *
     * @param context Context
     * @return Locale
     */
    @NonNull
    public static Locale getAppLocale(Context context) {
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { //7.0有多语言设置获取顶部的语言
            locale = context.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = context.getResources().getConfiguration().locale;
        }
        return locale;
    }

    /**
     * 获取用户设置的Locale
     *
     * @return Locale
     */
    @NonNull
    public static Locale getUserLocale(Context context) {
        String currentLanguage = SPUtils.getString(context, LocaleUtil.CURRENT_LANGUAGE, LANGUAGE_AUTO);
        Locale myLocale;
        switch (currentLanguage) {
            case LANGUAGE_CHINESE:
                myLocale = Locale.SIMPLIFIED_CHINESE;
                break;
            case LANGUAGE_ENGLISH:
                myLocale = Locale.ENGLISH;
                break;
            default:
                myLocale = getSystemLocale();
                break;
        }
        return myLocale;
    }

    /**
     * 保存设置的语言
     *
     * @param currentLanguage index
     */
    public static void setAppLanguage(@NonNull Context context, String currentLanguage) {
        Log.d("--------------", "setAppLanguage() called with: context = [" + context + "], currentLanguage = [" + currentLanguage + "]");
        Context appContext = context.getApplicationContext();
        SPUtils.put(context, LocaleUtil.CURRENT_LANGUAGE, currentLanguage);
        Locale myLocale = getUserLocale(context);

        // 本地语言设置
        if (LocaleUtil.needUpdateLocale(appContext, myLocale)) {
            LocaleUtil.updateLocale(appContext, myLocale);
        }

        restartApp(appContext);
    }

    /**
     * 重启app生效
     *
     * @param context
     */
    public static void restartApp(Context context) {
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 更新Locale
     *
     * @param context Context
     * @param locale  New User Locale
     */
    public static void updateLocale(Context context, Locale locale) {
        if (needUpdateLocale(context, locale)) {
            Configuration configuration = context.getResources().getConfiguration();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                LocaleList localeList = new LocaleList(locale);

                LocaleList.setDefault(localeList);
                configuration.setLocale(locale);
                configuration.setLocales(localeList);
                context = context.createConfigurationContext(configuration);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                configuration.setLocale(locale);
            } else {
                configuration.locale = locale;
            }
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            context.getResources().updateConfiguration(configuration, displayMetrics);
        }
    }

    /**
     * 判断需不需要更新
     *
     * @param context Context
     * @param locale  New User Locale
     * @return true / false
     */
    public static boolean needUpdateLocale(Context context, Locale locale) {
        return locale != null && !getAppLocale(context).equals(locale);
    }

    /**
     * 当系统语言发生改变的时候还是继续遵循用户设置的语言
     *
     * @param context
     * @param newConfig
     */
    public static void onConfigurationChanged(Context context, Configuration newConfig) {

        if (context == null) return;
        Context appContext = context.getApplicationContext();
        Locale locale;
        if (isAuto(context)) {
            locale = getLocaleFromConfiguration(newConfig);
            Configuration configuration = new Configuration(newConfig);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                configuration.setLocale(locale);
                LocaleList localeList = new LocaleList(locale);
                LocaleList.setDefault(localeList);
                configuration.setLocale(locale);
                configuration.setLocales(localeList);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                configuration.setLocale(locale);
            } else {
                configuration.locale = locale;
            }
            appContext.getResources().updateConfiguration(configuration, appContext.getResources().getDisplayMetrics());
        } else {
            //do nothing,keep original language
        }

    }

    public static Locale getLocaleFromConfiguration(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return configuration.getLocales().get(0);
        } /*else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
           configuration.setLocale();
        }*/ else {
            return configuration.locale;
        }
    }

    public static Context wrap(Context context) {
        Resources res = context.getResources();
        Configuration configuration = res.getConfiguration();
        Locale newLocale = getUserLocale(context);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(newLocale);
            LocaleList localeList = new LocaleList(newLocale);
            LocaleList.setDefault(localeList);
            configuration.setLocale(newLocale);
            configuration.setLocales(localeList);
            context = context.createConfigurationContext(configuration);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(newLocale);
            context = context.createConfigurationContext(configuration);

        } else {
            configuration.locale = newLocale;
            res.updateConfiguration(configuration, res.getDisplayMetrics());
        }
        return new ContextWrapper(context);
    }
}