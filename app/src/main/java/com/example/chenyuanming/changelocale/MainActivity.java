package com.example.chenyuanming.changelocale;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.tv_eng).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocaleUtil.setAppLanguage(v.getContext(), LocaleUtil.LANGUAGE_ENGLISH);
            }
        });
        findViewById(R.id.tv_auto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocaleUtil.setAppLanguage(v.getContext(), LocaleUtil.LANGUAGE_AUTO);
            }
        });
        findViewById(R.id.tv_zh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocaleUtil.setAppLanguage(v.getContext(), LocaleUtil.LANGUAGE_CHINESE);
            }
        });


        tv = findViewById(R.id.tv);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = getResources().getString(R.string.app_name);
                tv.setText(string);
                Toast.makeText(v.getContext(), LocaleUtil.getAppLocale(v.getContext()).getLanguage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    @CallSuper
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleUtil.wrap(newBase));
    }



//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        Log.d("MainActivity-------------", "onConfigurationChanged() called with: newConfig = [" + newConfig + "]");
//
//    }
}
