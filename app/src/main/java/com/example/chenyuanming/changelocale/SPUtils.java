package com.example.chenyuanming.changelocale;

import android.content.Context;
import android.content.SharedPreferences;

public class SPUtils {

    private static final String FILE_NAME = "com.nestia.living.base.utils.PREF_FILE_NAME";

    public static final String KEY_IS_AGENT_APP = "key_is_agent_app";
    public static final String KEY_FEEDBACK_ONLINE_POPUP_SHOWN = "key_feedback_online_popup_shown";
    public static final String KEY_SERVICE_FREQUENTLY_USED = "key_poi_frequently_used";
    public static final String KEY_SERVICE_TAB_RED_DOT = "key_service_tab_red_dot";
    public static final String KEY_CHAT_VERIFY_PHONE = "key_chat_verify_phone";

    public static final String KEY_AGENT_GURU_DIALOG_SHOWN = "key_agent_guru_dialog";
    public static final String KEY_AGENT_SYNCING_POPUP_SHOWN = "key_agent_syncing_popup_shown";
    public static final String KEY_AGENT_SYNC_IDEL_POPUP_SHOWN = "key_agent_sync_idel_popup_shown";

    private static SharedPreferences sPreferences;

    private SPUtils() {
        // nothing
    }

    public static void initialize(Context context) {
        sPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public static void put(Context context, String key, Object object) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        if (object instanceof String) {
            editor.putString(key, (String) object);
        } else if (object instanceof Integer) {
            editor.putInt(key, (Integer) object);
        } else if (object instanceof Boolean) {
            editor.putBoolean(key, (Boolean) object);
        } else if (object instanceof Float) {
            editor.putFloat(key, (Float) object);
        } else if (object instanceof Long) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, object.toString());
        }
        editor.apply();
    }

    public static String getString(Context context, String key, String defaultValue) {
        return getSharedPreferences(context).getString(key, defaultValue);
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        return getSharedPreferences(context).getBoolean(key, defaultValue);
    }

    public static int getInt(Context context, String key, int defaultValue) {
        return getSharedPreferences(context).getInt(key, defaultValue);
    }

    public static long getLong(Context context, String key, long defaultValue) {
        return getSharedPreferences(context).getLong(key, defaultValue);
    }


    public static void remove(Context context, String key) {
        getSharedPreferences(context).edit().remove(key).apply();
    }

    public static boolean contains(Context context, String key) {
        return getSharedPreferences(context).contains(key);
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return sPreferences == null ? context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE) : sPreferences;
    }
}